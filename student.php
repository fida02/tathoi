<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/student.css">
     <link rel="stylesheet" href="./css/homepage.css">
    <link rel="stylesheet" href="./css/font-awesome-4.7.0/css/font-awesome.css">
    <title>Result</title>
    <style>
        body{
            margin: 0;
            font-family: 'Roboto', sans-serif;
            background-color: #9ae49a;
}
        .container{
            border: 4px solid white;
            margin: 5% 10%;
            background-image: linear-gradient( 359.7deg, rgba(51,153,51,1) -4.5%, rgba(255,255,255,1) 89.8% );
       }
        .details{
    font-size: 20px;
    letter-spacing: 0.1em;
    background-image: linear-gradient( 359.7deg, rgba(51,153,51,1) -4.5%, rgba(255,255,255,1) 89.8% );
       }
        
        #homebutton{
            width: 120px;
            height: 40px;
            background-color:#0EB50D;
            transition: .5s;
            border: 1px solid white;
            margin-top: 5px;
            color: white;
            font-size: 20px;
            
        }
        #homebutton a{
            color: white;
            font-size: 20px;
            text-decoration: none;
             transition: .5s;
        }
        #homebutton:hover{
background-image: linear-gradient(to right,#0EB50D, #6eb20e);
            color: white;
             transition: .5s;
        }
        .print{
            font-weight: bold;
            font-family: Arial Narrow;
            
        }
        center{
            font-size: 40px;
            margin-top: 300px;
        }
        h4{
            font-size: 20px;
            text-align: center;
        }
        .heading{
            font-size: 25px;
            color: #0EB50D;
            margin-left: 25%;
            font-weight: bold;
            font-family: Arial Narrow;
        }
        .heading2{
            font-size: 30px;
            color: #0EB50D;
            margin-left: 22%;
            font-weight: bold;
            font-family:Arial Narrow;
        }
        .heading3{
            font-size: 20px;
            color: #0EB50D;
            margin-left: 38%;
            font-weight: bold;
            font-family:Arial Narrow;
            text-decoration: underline;
        }
        .heading4{
            font-size: 20px;
            color: #0EB50D;
            margin-left: 29%;
            font-weight: bold;
            font-family:Arial Narrow;
            
        }
        .heading5{
            font-size: 20px;
            color: #0EB50D;
            margin-left: 29.5%;
            font-weight: bold;
            font-family:Arial Narrow;   
        }
        span{
            color:antiquewhite;
            font-weight:bold;
            margin-left: 20px;
            font-family:Agency FB;
            font-size: 25px;
        }
       hr {
    border-top: 1px solid white;
} 
        .main{
    font-size: 18px;
    font-weight: bold;
    color:white;
    display: grid;
    grid-template-columns: 1fr 1fr;
    padding: 20px 25%;
    font-family:Arial Narrow;   
}
        .main h3{
        color: #0EB50D;
        font-family:Arial Narrow;   
        }
        .result p{
            color: white;
            margin-left: 43%;
        }

        
        
    </style>
</head>
<body>
    <?php
        include("init.php");

        if(!isset($_GET['class']))
            $class=null;
        else
            $class=$_GET['class'];
        $rn=$_GET['rn'];

        // validation
    
        if (empty($class) or empty($rn) or preg_match("/[a-z]/i",$rn)) {
            if(empty($class))
                echo '<p class="error">Please select your class</p>';
            if(empty($rn))
                echo '<p class="error">Please enter your roll number</p>';
            if(preg_match("/[a-z]/i",$rn))
                echo '<p class="error">Please enter valid roll number</p>';
            exit();
        }

        $name_sql=mysqli_query($conn,"SELECT `name` FROM `students` WHERE `rno`='$rn' and `class_name`='$class'");
        while($row = mysqli_fetch_assoc($name_sql))
        {
        $name = $row['name'];
        }

        $result_sql=mysqli_query($conn,"SELECT `p1`, `p2`, `p3`, `p4`, `p5`, `marks`, `percentage` FROM `result` WHERE `rno`='$rn' and `class`='$class'");
        while($row = mysqli_fetch_assoc($result_sql))
        {
            $p1 = $row['p1'];
            $p2 = $row['p2'];
            $p3 = $row['p3'];
            $p4 = $row['p4'];
            $p5 = $row['p5'];
            $mark = $row['marks'];
            $percentage = $row['percentage'];
        }
        if(mysqli_num_rows($result_sql)==0){
        echo "<center>Sorry!<br>No result found</center>";
        
            exit();
        }
    ?>

    <div class="container">
        <div class="details">
            <div class="logo">
            <img style="border-radius:50%;margin-left:45%;height:65px; width:70px;" src="images/clg.png" alt="" class="logo"></div>
            <span class="heading">Bangladesh Technical Education Board,Dhaka</span><br>     
            <span class="heading2"> Chittagong Polytechnic institute,Chittagong</span><br>
            <span class="heading3">ACADEMIC TRANSCRIPT</span><br>
            <span class="heading4">DIPLOMA-IN-ENGINEERING(Duration:4-Years)</span><br>
            <span class="heading5">FIRST SEMISTER FINAL EXAMINATION,2019</span><hr><br>
            
            <span>Name: <?php echo $name; ?> </span><br>
            <span>Technology: <?php echo $class; ?></span> <br>
            <span>Roll No: <?php echo $rn; ?></span> <br>
        </div>
        <div class="main">
            <div style= ""class="s1">
                <h3><p>Subjects</p></h3>
                <hr>
                <p>Computer Application</p>
                <hr>
                <p>Electrical Engineering Fundamentals</p>
                <hr>
                <p>Mathmatics-1</p>
                <hr>
                <p>Physics-1</p>
                <hr>
                <p>Bangla</p>
                <hr>
            </div>
            <div class="s2">
                <h3><p>Marks</p></h3>
                <hr>
                <?php echo '<p>'.$p1.' '; if ($p1 > 79){ echo "(A+) (4.00)"; }else if ($p1 > 74 && $p1 < 80){ echo "(A) (3.75)";}else if ($p1 > 69 && $p1 < 76){ echo "(A-) (3.5)";}else if ($p1 > 64 && $p1 < 71){ echo "(B+) (3.25)";}else if ($p1 > 59 && $p1 < 66){ echo "(B) (3.00)";}else if ($p1 > 54 && $p1 < 61){ echo "(B-) (2.75)";}else if ($p1 > 49 && $p1 < 56){ echo "(C+) (2.5)";}else if ($p1 > 44 && $p1 < 51){ echo "(C) (2.25)";}else if ($p1 > 39 && $p1 < 46){ echo "(D) (2.00)";}else if($p1 < 40){ echo "(F) (0.00)";} ?></p>
                <hr>
                <?php echo '<p>'.$p2.' '; if ($p2 > 79){ echo "(A+) (4.00)"; }else if ($p2 > 74 && $p2 < 80){ echo "(A) (3.75)";}else if ($p2 > 69 && $p2 < 76){ echo "(A-) (3.5)";}else if ($p2 > 64 && $p2 < 71){ echo "(B+) (3.25)";}else if ($p2 > 59 && $p2 < 66){ echo "(B) (3.00)";}else if ($p2 > 54 && $p2 < 61){ echo "(B-) (2.75)";}else if ($p2 > 49 && $p2 < 56){ echo "(C+) (2.5)";}else if ($p2 > 44 && $p2 < 51){ echo "(C) (2.25)";}else if ($p2 > 39 && $p2 < 46){ echo "(D) (2.00)";}else if($p2 < 40){ echo "(F) (0.00)";} ?></p>
                <hr>
                <?php echo '<p>'.$p3.' '; if ($p3 > 79){ echo "(A+) (4.00)"; }else if ($p3 > 74 && $p3 < 80){ echo "(A) (3.75)";}else if ($p3 > 69 && $p3 < 76){ echo "(A-) (3.5)";}else if ($p3 > 64 && $p3 < 71){ echo "(B+) (3.25)";}else if ($p3 > 59 && $p3 < 66){ echo "(B) (3.00)";}else if ($p3 > 54 && $p3 < 61){ echo "(B-) (2.75)";}else if ($p3 > 49 && $p3 < 56){ echo "(C+) (2.5)";}else if ($p3 > 44 && $p3 < 51){ echo "(C) (2.25)";}else if ($p3 > 39 && $p3 < 46){ echo "(D) (2.00)";}else if($p3 < 40){ echo "(F) (0.00)";} ?></p>
                <hr>
                <?php echo '<p>'.$p4.' '; if ($p4 > 79){ echo "(A+) (4.00)"; }else if ($p4 > 74 && $p4 < 80){ echo "(A) (3.75)";}else if ($p4 > 69 && $p4 < 76){ echo "(A-) (3.5)";}else if ($p4 > 64 && $p4 < 71){ echo "(B+) (3.25)";}else if ($p4 > 59 && $p4 < 66){ echo "(B) (3.00)";}else if ($p4 > 54 && $p4 < 61){ echo "(B-) (2.75)";}else if ($p4 > 49 && $p4 < 56){ echo "(C+) (2.5)";}else if ($p4 > 44 && $p4 < 51){ echo "(C) (2.25)";}else if ($p4 > 39 && $p4 < 46){ echo "(D) (2.00)";}else if($p4 < 40){ echo "(F) (0.00)";} ?></p>
                <hr>
                <?php echo '<p>'.$p5.' '; if ($p5 > 79){ echo "(A+) (4.00)"; }else if ($p5 > 74 && $p5 < 80){ echo "(A) (3.75)";}else if ($p5 > 69 && $p5 < 76){ echo "(A-) (3.5)";}else if ($p5 > 64 && $p5 < 71){ echo "(B+) (3.25)";}else if ($p5 > 59 && $p5 < 66){ echo "(B) (3.00)";}else if ($p5 > 54 && $p5 < 61){ echo "(B-) (2.75)";}else if ($p5 > 49 && $p5 < 56){ echo "(C+) (2.5)";}else if ($p5 > 44 && $p5 < 51){ echo "(C) (2.25)";}else if ($p5 > 39 && $p5 < 46){ echo "(D) (2.00)";}else if($p5 < 40){ echo "(F) (0.00)";} ?></p>
                <hr>
            </div>
        </div>
       
-
        <div class="result">
            <?php if ($p1 < 40 || $p2 < 40 || $p3 < 40 || $p4 < 40 || $p5 < 40){ echo '<p>You have failed!</p>'; }else{ echo '<p>You have Passed!</p>'; } ?>
            <?php echo '<p>Total Marks:&nbsp'.$mark.'</p>';?>
            <?php echo '<p>Percentage:&nbsp'.$percentage.'%</p>';?>
        </div>

        <div class="button">
            <button id="homebutton" class="print" onclick="window.print()">Print Result</button>
        </div>
        <div class="button">
            <button id="homebutton"><a href="index.html"> HOME </a> </button>
        </div>
    </div>
</body>
</html>